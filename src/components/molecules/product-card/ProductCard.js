import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { Button, CardActionArea, CardActions } from "@mui/material";
import "./ProductCard.css";
import { Link, useHistory } from "react-router-dom";

function ProductCard({ key, productData, type }) {
  let history = useHistory();
  return (
    <Card
      sx={{
        width: type === "detail-card" ? "90%" : 250,
        height: "100%",
        marginX: 2,
        padding: 5,
        marginTop: 5,
        backgroundColor: "#efefef",
      }}
    >
      <CardMedia
        component="img"
        height="200"
        width="300"
        image={productData.image}
        alt=""
        style={{ objectFit: "contain" }}
      />
      <CardContent>
        <Typography gutterBottom variant="h7" component="div">
          {productData.title}
        </Typography>
        {type === "detail-card" && (
          <Typography variant="h7" color="gray">
            {productData.description}
          </Typography>
        )}
        <Typography variant="h7" color="red">
          Price : {productData.price}
        </Typography>
      </CardContent>
      <CardActions style={{ justifyContent: "space-between" }}>
        {type !== 'detail-card' && <Button
          size="small"
          style={{ backgroundColor: "darkslategrey", color: "white" }}
          onClick={() =>
            history.push({
              pathname: "/product-details",
              state: { productData },
            })
          }
        >
          Details
        </Button>}

        <Button
          size="small"
          style={{ backgroundColor: "darkslategrey", color: "white" }}
        >
          Add to cart
        </Button>
      </CardActions>
    </Card>
  );
}

export default ProductCard;
