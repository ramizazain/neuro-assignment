import React from "react";
import "./Header.css";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";

function Header({isOpened, setIsOpened}) {
  return (
    <div className="header">
      <div className="icon" onClick={() => setIsOpened(!isOpened)}>
        {isOpened ? <ChevronLeftIcon /> : <MenuIcon />}
      </div>
    </div>
  );
}

export default Header;
