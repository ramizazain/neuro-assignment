import React, { useState } from "react";
import "./SideBar.css";

function SideBar({ isOpened, categoryList, onClickCategory }) {
  return (
    <aside className={`${isOpened ? "opened" : ""} drawer`}>
      {categoryList?.map((category, index) => (
        <div
          className="cat-item"
          key={index}
          onClick={() => onClickCategory(category)}
        >
          {category}
        </div>
      ))}
    </aside>
  );
}

export default SideBar;
