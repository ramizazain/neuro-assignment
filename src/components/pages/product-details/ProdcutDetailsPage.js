import axios from "axios";
import React, { useEffect, useState } from "react";
import Header from "../../molecules/header/Header";
import ProductCard from "../../molecules/product-card/ProductCard";
import SideBar from "../../molecules/side-bar/SideBar";
import "./ProductDetailsPage.css"

function ProductDetailsPage({location}) {
    const [isOpened, setIsOpened] = useState(false);
  const [categoryList, setCategoryList] = useState(null);
  useEffect(() => {
    axios.get('https://fakestoreapi.com/products/categories')
      .then(data => {
        setCategoryList(data.data)
      });
  }, []);
  return (
    <div className="App">
      <Header isOpened={isOpened} setIsOpened={(value) => setIsOpened(value)} />
      <ProductCard type={'detail-card'} productData={location.state.productData}/>
    </div>
  );
}

export default ProductDetailsPage;
