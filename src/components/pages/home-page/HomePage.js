import React, { useEffect, useState } from "react";
import axios from "axios";
import "./HomePage.css";
import Header from "../../molecules/header/Header";
import SideBar from "../../molecules/side-bar/SideBar";
import ProductList from "../../organisms/product-list/ProductList"

function HomePage() {
  const [isOpened, setIsOpened] = useState(false);
  const [categoryList, setCategoryList] = useState(null);
  const [productList, setProductList] = useState(null);
  const onClickCategory = (categoryName) => {
    axios
      .get(`https://fakestoreapi.com/products/category/${categoryName}`)
      .then((data) => setProductList(data.data));
  };
  useEffect(() => {
    axios.get("https://fakestoreapi.com/products/categories").then((data) => {
      setCategoryList(data.data);
    });
  }, []);

  return (
    <div className="App">
      <Header isOpened={isOpened} setIsOpened={(value) => setIsOpened(value)} />
      <div className="content">
        <SideBar
          isOpened={isOpened}
          categoryList={categoryList}
          onClickCategory={onClickCategory}
        />
        <ProductList productList={productList} />
      </div>
    </div>
  );
}

export default HomePage;
