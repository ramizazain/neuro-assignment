import React from "react";
import ProductCard from "../../molecules/product-card/ProductCard";
import "./ProductList.css";

function ProductList({productList}) {
  return (
    <div className="main">
      {productList?.map((product, index) => (
        <ProductCard key={index} productData={product}/>
      ))}
    </div>
  );
}

export default ProductList;
