import React from "react";
import {BrowserRouter as Router, Route} from 'react-router-dom'
import HomePage from "./components/pages/home-page/HomePage";
import ProductDetailsPage from "./components/pages/product-details/ProdcutDetailsPage"

function App() {
  return (
    <Router>
      <Route path="/" exact component={HomePage}/>
      <Route path="/product-details" component={ProductDetailsPage}/>
    </Router>
  );
}

export default App;
